# JIRA worklog CLI

Usage:
```sh
./jira_cli.rb
```
For now to get this work outside this directory needs to install these gems:
```sh
gem install mechanize colorize highline
```
This will be fixed with releasing a gem
#### To make a symlink to a binary use this:
```sh
ln -s full_path_to_/jira_cli.rb /usr/local/bin/jira
```

Needs to create file `jira_logger_categoriess.json` with categories for worklog in the HOME directory.
File example
```json
{
  "default": "New Feature/CR implementation",
  "list": [
    "Code Review",
    "Defect fixing",
    "Environment setup",
    "HR Comments",
    "HR Exit Interview",
    "HR Interview",
    "Investigations",
    "Knowledge Transfer",
    "New Feature/CR implementation"
  ]
}
```

#### Help:
- JIRA Url should be like that: `https://jira.mycompany.com/`

Examples:

![](http://take.ms/0SZc7)

![](http://take.ms/aUsFk)
